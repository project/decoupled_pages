<?php

namespace Drupal\decoupled_pages\Exception;

/**
 * Indicates that a decoupled page route was improperly declared.
 */
final class DataProviderException extends ImplementationException {}
