<?php

namespace Drupal\decoupled_pages\Exception;

/**
 * Indicates that a module using decoupled pages was improperly implemented.
 */
abstract class ImplementationException extends \LogicException {}
